import { ServiceStub } from './ServiceStub';
import { SecondServiceStub } from './SecondServiceStub';
import { HistoryStub } from './HistoryStub';

export class CalculationHistoryForMultiServiceStub {
    constructor(uncalculatedFees, secondUncalculatedFees) {
        this.historyMap = new Map([
            [ServiceStub.name, new HistoryStub(uncalculatedFees)],
            [SecondServiceStub.name, new HistoryStub(secondUncalculatedFees)]
        ]);
    }

    retrieveHistory(service) {
        return this.historyMap.get(service.constructor.name);
    }

    verifyAppliedSumForService(expectedSum, serviceClass) {
        this.historyMap.get(serviceClass.name).verifyAppliedSum(expectedSum);
    }
}